## Personal i3 config for Linux
Current Version: `1.2.0`

### Installation guide
0. Update your package manager (On Ubuntu run `sudo apt update`)
1. Install the following packages: `i3 rofi lxappearance feh x11-xserver-utils scrot imagemagick dbus flameshot xbacklight simplescreenrecorder volumeicon nm-applet blueman-applet clipit`.
2. Copy this i3 folder to `.config/.i3` or `.i3`  folder in home directory (if it already exists, replace it).
3. Restart your pc. On login screen click on the settings icon and choose i3.
4. Modify your default UI design settings with lxappearance.
5. Copy some wallpaper images to wallpapers folder (the name of the files does not matter).
6. Select a lock screen image and copy it to lockscreen folder as image.png.

### Further Information
- Restart i3 with $mod+Shift+r in order to apply any change.
- Always consider rebooting your PC if something does not work.
- For Ubuntu you have to install `volumeicon-alsa blueman network-manager` instead of `volumeicon nm-applet blueman-applet`
- On errors controll your `$configPath` in `config`

### References
- Visit http://i3wm.org/docs/userguide.html for a complete i3 reference.
- Visit https://i3wm.org/i3status/manpage.html for a complete i3status reference.
- Visit https://www.reddit.com/r/unixporn/comments/3358vu/i3lock_unixpornworthy_lock_screen/ for code and more options regarding the lock screen (lock.sh).
